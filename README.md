**Job Recommendation System V1 (jobs to jobs Recommendation)**

## Clone the Repo
https://github.com/harshgupta9723/job_recommendation.git

## Creating virtual environment
Create a virtual enviroment and install requirements.txt file

```pip3 install -r requirements.txt```

## Task:-
Given a job Id find out the similar jobs for a particular jobs ID and recommend them

If anyone passes the job_id for getting a recommendation for a particular job id so

- First, we fetch the data for the given job_id like we fetch (job_title, job_description, job_category, zip code, city, state, and some more details according to the need)

- Then for getting a recommendation we store the values like job_category, state, city, and then fetch the data with these clauses and get the data that has the same category, state, city, and more.

- Then we check the similarities between the given job_id description and the fetched job_id description and we clean the data, then check the similarities using the **Cosin Similarity matrix** and we are only taking these jobs that have more than **50% of similarity with the given job id** and we show those jobs as a recommended jobs for the given job_id

## Code Structure

- **db_recommendation.py**

This script is responsible for making connection with MySql and Psql databases, and getting the data and setting the data into Redis for pre-cache

- **recommendation_api.py**

This script is responsible for fetching the user data for the applied job_id, and then fetch the same jobs for the user applied job_id and return top-recommended job for the user

- **client_recommendation.py**

This script is responsible for fetching jobs_id's and then pass these jobs_id's into api and set the keys, values in the Redis

- **get_redis_data.py**

This Script is responsible for getting the data stored in Redis

## Api's

- **redis_jobs**

This api is for fetching all the values of the keys which matches the specified pattern. The values are fetched from redis database.

- **jobs**

This api is for showing all the recommended jobs to the user. Recommendations are based on the similarity between user entered job and jobs already in the database. 




