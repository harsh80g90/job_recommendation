"""
client_recommendation.py
- This script is responsible for fetching jobs_id's and then pass
these jobs_id's into api and set the keys, values in the Redis
"""

import requests
import json
from db_recommendation import Database
import os

database = Database()

# get job ids from the database


class Client:
    def __init__(self):
        print("Client Code")

    def get_job_id(self):

        db = database.get_connection_psql()

        query = """select job_id from jobs.synthetic_job"""

        raw_user_data = database.get_data(db, query)

        return raw_user_data

    def main(self):

        # get all the job_id's
        job_id = self.get_job_id()

        # process all the job_id to get the recommended jobs
        for id in job_id:
            print(id[0])

            # pass the job_id's in out recommendation api
            result = requests.post(
                'http://'+os.getenv("PDIP")+':5080/jobs',
                data={'job_id': id[0]})
            json_data = json.loads(result.text)

            # getting kyes
            key = json_data['key']
            # getting values
            value = json_data['recommended_jobs']

            try:
                # set the value in redis
                database.set_value(key, str(value))
            except Exception as e:
                print("fail", e)
                pass


if __name__ == "__main__":
    client = Client()
    client.main()
