"""
db_recommendation.py
This script is responsible for making connection with MySql and Psql
databases,and getting the data and setting the data into Redis for
pre-cache
"""


import os
from sqlalchemy import create_engine
import psycopg2
import redis
import sys


class Database:

    def __init__(self):
        print("Database")

    # psql data base connection
    def get_connection_psql(self):

        connection = psycopg2.connect(host=os.getenv("PDBHOST"),
                                      user=os.getenv("PDBUSER"),
                                      password=os.getenv("PDBPASS"),
                                      dbname=os.getenv("PDBNAME"),
                                      port=os.getenv("PDBPORT"))

        return connection

    # mysql database connection
    def get_connection_sql(self):

        user = os.getenv("DBUSER")
        pwd = os.getenv("DBPASS")
        host = os.getenv("DBHOST")
        database = os.getenv("DBNAME")
        dbport = os.getenv("DBPORT")

        con_string = "mysql+pymysql://{}:{}@{}:{}/{}".format(
            user, pwd, host, dbport, database)

        # create engine
        sqlEngine = create_engine(con_string, pool_recycle=3600)
        db = sqlEngine.raw_connection()

        return db

    # redis database connection
    def get_redis(self):

        redis_conn = redis.StrictRedis('localhost',
                                       6379,
                                       decode_responses=True)

        return redis_conn

    # get values from redis database
    def get_value(self, key):

        r = self.get_redis()

        key = str(key)
        try:
            value = r.get(key)
        except Exception as e:
            sys.exit("error in execution..", e)

        return value

    # set values in redis database
    def set_value(self, job_id, job_detail):

        r = self.get_redis()

        try:
            value = r.set(job_id, job_detail)
        except Exception as e:
            sys.exit("error in execution..", e)

        return value

    # get data
    def get_data(self, db, query):

        cur = db.cursor()

        data = []
        cur.execute(query)

        job_data = cur.fetchall()

        for row in job_data:
            data.append(row)

        cur.close()

        return data
