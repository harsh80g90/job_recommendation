from db_recommendation import Database
database = Database()


class KeysRecommendation:
    def main(self):

        r = database.get_redis()

        keys = r.keys()
        print(keys)


if __name__ == "__main__":
    keys_recommendation = KeysRecommendation()
    keys_recommendation.main()
