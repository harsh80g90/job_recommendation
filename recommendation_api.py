"""
recommendation_api.py
- This script is responsible for fetching the user data for the applied job_id,
  and then fetch the same jobs for the user applied job_id and return
  top-recommended job for the user
"""

from flask import Flask, request, jsonify
from pandas.core.accessor import register_index_accessor
from db_recommendation import Database
import nltk
from nltk.corpus import stopwords
import re
import pandas as pd
import textdistance
import json
import os
pd.options.mode.chained_assignment = None

# # Downloading the stop words list
nltk.download('stopwords')
# # Loading the stop words in english
stop_words = list(stopwords.words('english'))

app = Flask(__name__)

database = Database()


class RecommendationApi:
    def job_cleaner(self, text):
        """
            text: a string
            return: cleaned initial string
        """

        REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
        BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
        STOPWORDS = set(stopwords.words('english'))

        text = str(text)
        text = text.lower()  # lowercase text
        # replace REPLACE_BY_SPACE_RE symbols by space in text
        text = REPLACE_BY_SPACE_RE.sub(' ', text)
        # delete symbols which are in BAD_SYMBOLS_RE from text
        text = BAD_SYMBOLS_RE.sub('', text)
        text = ' '.join(word for word in text.split()
                        if word not in STOPWORDS)  # delete stopwords

        return text

    def get_jobs_data(self, db, job_id):
        """ 
            input
                - db (database connection)
                - job_id (int job_id)
            output
                - return dataframe with simillar jobs with respect to the
                given job_id
        """

        query_1 = f"""select
                html_job_description,
                jobs.city.name as city_name,
                industry
                from jobs.job join jobs.job_location on
                jobs.job.id = jobs.job_location.job_id
                join jobs.city on jobs.job_location.city_id = jobs.city.id
                where jobs.job.id = {job_id}
                """

        user_details = database.get_data(db, query_1)

        city_name = user_details[0][1]
        industry = user_details[0][2]

        condition = ""
        if not industry or not city_name:
            if industry == None:
                condition += f"""where jobs.city.name = '{city_name}' group by
                jobs.job.id, city_name"""
            elif city_name == None:
                condition += f"where industry = '{industry}' group by jobs.job.id"
        else:
            condition += f"""where jobs.city.name = '{city_name}'
             and industry = '{industry}'"""

        query = f"""select
            job.id,
            job_title,
            html_job_description,
            jobs.city.name as city_name,
            industry
            from jobs.job join jobs.job_location on
            jobs.job.id = jobs.job_location.job_id
            join jobs.city on jobs.job_location.city_id = jobs.city.id
            {condition}
            """

        details = database.get_data(db, query)

        main_df = pd.DataFrame(details, columns=[
            "job_id",
            "job_title",
            "job_description",
            "city_name",
            "industry"])

        # db.close()

        return main_df

    def get_similar_jobs(self, user_job_desc, job_description):
        """
            input
                - user_job_desc (text : user job description)
                - job_description (text : fetched job desctiption)
            output
                - return a list with the similarity scores between
                 user_job_desc and job_description
        """
        similarity = []

        for job in job_description:
            job_desc = self.job_cleaner(job)
            simm = textdistance.cosine.normalized_similarity(
                user_job_desc, job_desc)
            similarity.append(simm)
        return similarity

    def generate_redis_key(self, job_id, industry, city):

        key_string = f"{str(industry)}_{str(city)}_{str(job_id)}"

        final_string = key_string.replace(" ", "_")

        return final_string

    def main(self, job_id):

        # making connectin with psql database
        db = database.get_connection_psql()

        # get user job details from job_id (job_id refers to a job applied by
        # a user).
        job_details = self.get_jobs_data(db, int(job_id))

        # get user data (job id refers to job applied by a user)
        user_data = job_details.loc[job_details['job_id'] == int(job_id)]

        # get user applied job description
        user_job_description = user_data['job_description']

        # key for redis
        try:
            industry = list(user_data['industry'])[0]
            city = list(user_data['city_name'])[0]
        except Exception as e:
            print("Error Occured", e)
            industry = ""
            city = ""
            pass

        # making keys for redis database
        redis_key = self.generate_redis_key(job_id, industry, city)

        # cleaning job description to calculate similarity.
        clean_user_job = self.job_cleaner(user_job_description)

        # get list of job descriptions
        list_job_descriptions = job_details["job_description"]

        # find similarity between user job description and list of job
        # descriptions
        similarity = self.get_similar_jobs(
            clean_user_job, list_job_descriptions)

        job_details["similarity"] = similarity

        # code to remove duplicates in the recommended jobs
        unique_job_details = job_details.drop_duplicates('similarity')

        # query job desc data frame return job details where sim > 50
        similar_job_details = unique_job_details[
            unique_job_details["similarity"].between(
                0.50, 0.99)]

        # descriptions are not including in results
        similar_job_details = similar_job_details.drop(
            ['job_description'], axis=1)
        similar_job_details = similar_job_details.reset_index()

        # converting pandas Dataframe to json
        result = similar_job_details.to_json(orient="index")

        value = json.loads(result)

        return redis_key, value

    def get_jobs(self, key_pattern):

        r = database.get_redis()

        keys = r.keys('*'+str(key_pattern)+'*')

        json_data = []
        for key in keys:
            try:
                data = database.get_value(key)
                json_data.append(data)
            except Exception as e:
                print("fail", e)
                pass

        return json_data


app = Flask(__name__)

api = RecommendationApi()


@app.route('/redis_jobs', methods=['POST', 'GET'])
def redis_jobs():

    key = request.form.get("key")

    try:
        value = api.get_jobs(key)
    except Exception as e:
        print("error occured", e)
        value = {}

    result_dict = {"recommended_jobs": value}

    return jsonify(result_dict)


@app.route('/jobs', methods=['POST', 'GET'])
def jobs():

    job_id = request.form.get("job_id")

    try:
        key, value = api.main(job_id)
    except Exception as e:
        print("error occured", e)
        key = ''
        value = {}

    result_dict = {"recommended_jobs": value,
                   "key": key}

    return jsonify(result_dict)


if __name__ == "__main__":
    # app.run(host="127.0.0.1", port=5080)
    app.run(host=os.getenv("PDIP"), port=5080)
    # app.run(port=5080)
