'''This script is responsible for unit testing recommendation_api.py file.
The script tests if the job descriptions are cleaned,desired data is being
fetched from database, similarity among jobs, right redis keys are
generated and the whole recommendation_api.py file is runnning smmothly.'''

import unittest
import recommendation_api
from recommendation_api import RecommendationApi
# from db_recommendation import Database

# database = Database()
api = RecommendationApi()
# db = database.get_connection_psql()


class TestApi(unittest.TestCase):
    # Function to test if the data is being cleaned or not.
    def test_job_cleaner(self):

        text = "</p><p><u>Bike Requirements:</u></p><p>Be"

        self.assertEqual(api.job_cleaner(
            text), 'ppubike requirements u ppbe')

        text = "Uber Eats background check</li><li>Vehicles"
        self.assertEqual(api.job_cleaner(
            text), 'uber eats background check lilivehicles')

        text = "!@ya&**m&^a>?>n"
        self.assertEqual(api.job_cleaner(text), 'yaman')

        text = ""
        self.assertEqual(api.job_cleaner(text), '')

        text = "job recommendation"
        self.assertEqual(api.job_cleaner(text), 'job recommendation')

    # def test_get_jobs_data(self):

    #     data = [[26, 'Walmart Store Lead', 'Ackermanville', 'WalMart'], [
    #         27, 'Jobs near me - Truck Driver CDL A - Lease Purchase',
    #  'Ackermanville', 'WalMart']]

    #     df = pd.DataFrame(
    #         data, columns=['job_id', 'job_title', 'city_name', 'industry'])

    #     self.assertEqual(
    #         type(api.get_jobs_data(db, 26)), type(df))

    #     data = [[27, 'Jobs near me - Truck Driver CDL A - Lease Purchase',
    #              'Ackermanville', 'WalMart']]

    #     df = pd.DataFrame(
    #         data, columns=['job_id', 'job_title', 'city_name', 'industry'])

    #     self.assertEqual(
    #         type(api.get_jobs_data(db, 27)), type(df))

    #     data = [[3145, 'Jobs near me - Truck Driver CDL A - Lease Purchase',
    #              'Ackermanville', 'WalMart']]

    #     df = pd.DataFrame(
    #         data, columns=['job_id', 'job_title', 'city_name', 'industry'])

    #     self.assertEqual(
    #         type(api.get_jobs_data(db, 26)), type(df))

    def test_get_similar_jobs(self):

        # function to test similarity among jobs

        lis = [0.24253562503633297, 0.0, 0.24253562503633297,
               0.0, 0.24253562503633297, 0.24253562503633297,
               0.0, 0.0, 0.0, 0.24253562503633297, 0.24253562503633297,
               0.0, 0.0, 0.0, 0.0, 0.0, 0.24253562503633297]

        self.assertEqual(api.get_similar_jobs(
            'jobRecommendation', 'jobRecommendation'), lis)

        lis = [0.5773502691896258, 0.5773502691896258, 0.0]
        self.assertEqual(api.get_similar_jobs(
            'job', 'jbo'), lis)

        lis = [0.0, 0.0, 0.0, 0.0]
        self.assertEqual(api.get_similar_jobs(
            'NULL', 'null'), lis)

        self.assertEqual(type(api.get_similar_jobs(
            'recommendation', 'Recomm')), type(lis))

        lis = [[44, 2]]
        self.assertEqual(type(api.get_similar_jobs(
            'recomm', 'Recomm')), type(lis))

    def test_generate_redis_key(self):

        # function to test if redis keys are generated in right format.

        self.assertEqual(api.generate_redis_key(
            12, 'Shipt', 'Abbeville'), 'Shipt_Abbeville_12')

        self.assertEqual(api.generate_redis_key(
            541, '', 'Abington'), '_Abington_541')

        self.assertEqual(api.generate_redis_key(
            1, '', ''), '__1')

        self.assertEqual(api.generate_redis_key(
            253688, 'Walmart', ''), 'Walmart__253688')

        self.assertEqual(api.generate_redis_key(
            289147, 5482, ''), '5482__289147')

    # def test_main(self):

    #     # function to test if the main function is returning right data.

    #     tup = ('WalMart_Ackermanville_26', {})

    #     self.assertEqual(api.main(26), tup)

    #     self.assertEqual(type(api.main(26)), type(tup))

    #     tup = ([1, 2, 3], [4], [6, 7])
    #     self.assertEqual(type(api.main(26)), type(tup))

    #     tup = ()
    #     self.assertEqual(type(api.main(26)), type(tup))

    def test_job_recommendation_statuscode(self):
        # function to test status code of api.
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/jobs')
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_job_recommendation_contenttype(self):
        # function to test content type of api
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/jobs')
        self.assertEqual(response.content_type, "application/json")

    def test_job_recommendation_data(self):
        # function to test data returned by api
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/jobs')
        self.assertTrue(b'recommended_jobs' in response.data)

    def test_job_redis_jobs_statuscode(self):
        # function to test status code of api.
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/redis_jobs')
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_redis_jobs_contenttype(self):
        # function to test content type of api
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/redis_jobs')
        self.assertEqual(response.content_type, "application/json")

    def test_redis_jobs_data(self):
        # function to test data returned by api
        tester = recommendation_api.app.test_client(self)
        response = tester.get('/redis_jobs')
        self.assertTrue(b'recommended_jobs' in response.data)


if __name__ == '__main__':
    unittest.main()
